### This is a Scandiweb Test project implementation

* It was implemented and tested on XAMPP

* To work this project needs a MySQL database named scandiweb (charset is utf8mb4_unicode_ci) with table products
* The structure of this table is
* id - primary index
* sku - unique index (varchar(14))
* name - varchar(100)
* price - double
* type - enum('DVD-Disc','Book','Furniture')
* extra - varchar(15)
