<?php
interface IMySQLConnection
{
	public function insert($query);
	public function select($query);
	public function delete($query);
	public function close();
}