<?php
include_once "Request.php";
include_once "Router.php";
include_once "Product.php";
include_once "DVD.php";
include_once "Book.php";
include_once "Furniture.php";
include_once "MySQLConnection.php";
$router = new Router(new Request()); // Router object
$mysql = new MySQLConnection("localhost", "root", "", "scandiweb"); // Object to handle mysql operations

// Route to check if server is ready
$router->get("/", function($request) {
	return "Main Page of API works fine!";
});

// Route to get all products (does not need any argument)
$router->get("/products", function($request) use(&$mysql) {
	$query = "SELECT * FROM products WHERE 1 ORDER BY id DESC";
	$result = $mysql->select($query);

	return json_encode($result);
});

/*
  Route to create new product
  Arguments:
  sku: sku string
  name: name of a product
  price: price of a product
  type: type of a product (1 - DVD-Disc, 2 - Book, 3 - Furniture)
  extra: extra information according to product's type
*/
$router->post("/product", function($request) use(&$mysql) {
	$body = $request->getBody();

	$newProduct;
	$type = intval($body["type"]);
	if ($type === 1) $newProduct = new DVD(null, $body["sku"], $body["name"], $body["price"], $body["extra"]);
	else if ($type === 2) $newProduct = new Book(null, $body["sku"], $body["name"], $body["price"], $body["extra"]);
	else if ($type === 3)
	{
		try
		{
			$newProduct = new Furniture(null, $body["sku"], $body["name"], $body["price"], $body["extra"]);
		}
		catch (InvalidArgumentException $e)
		{
			http_response_code(400);
			return $e->getMessage();
		}
	}
	else
	{
		http_response_code(400);
		return "Incorrect type";
	}
	$selectQuery = Product::find(null, $newProduct->getSku());

	if (sizeof($mysql->select($selectQuery)) > 0)
	{
		http_response_code(400);
		return "SKU already exists";
	}

	return $mysql->insert($newProduct->create());
});

// Route to delete a product. Takes argument ids, which must be JSON array of products ids to delete
$router->post("/products/delete", function($request) use(&$mysql) {
	$idArray = json_decode($request->getBody()["ids"]);

	$stringIds = implode(", ", $idArray);
	$query = "DELETE FROM products WHERE id IN (".$stringIds.")";

	return $mysql->delete($query);
});