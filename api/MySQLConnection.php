<?php
include_once "IMySQLConnection.php";


/**
 * This is a class implemented to deal with MySQL connection
 * 
 * @param string $server Server URI of a MySQL server
 * @param string $username Username to access MySQL server
 * @param string $password Password to access MySQL server
 * @param string $db Database name to connect
 */
class MySQLConnection implements IMySQLConnection
{
	private $connection;
	private $server;
	private $username;
	private $password;
	private $db;

	function __construct($server, $username, $password, $db)
	{
		$this->server = $server;
		$this->username = $username;
		$this->password = $password;
		$this->db = $db;

		$this->connection = new mysqli($server, $username, $password, $db);

		if ($this->connection->connect_error) die("Connection failed ".$this->connection->connect_error);
	}

	// Function to process query of type INSERT
	public function insert($query)
	{
		if ($this->connection->query($query) === TRUE)
		{
			return $this->connection->insert_id;
		}
		else throw new Exception("Cannot insert new row. Error: ".$this->connection->error);
	}

	// Function to process query of type SELECT
	public function select($query)
	{
		$result = $this->connection->query($query);
		if ($result === FALSE) throw new Exception("Cannot select rows. Error: ".$this->connection->error);
		$array = array();

		while($row = $result->fetch_assoc())
		{
			array_push($array, $row);
		}

		return $array;
	}

	// Function to process query of type DELETE
	public function delete($query)
	{
		if ($this->connection->query($query) === TRUE)
		{
			return "Success";
		}
		else throw new Exception("Cannot delete rows. Error: ".$this->connection->error);
	}

	// Function to close connection with the MySQL server
	public function close()
	{
		$this->connection->close();
	}
}