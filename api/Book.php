<?php
/**
 * This is a class implemented to deal with products
 * 
 * @param int|null $id id of a product which must be null if it is a new product
 * @param string $sku SKU of a product from 0 to 14 characters length
 * @param string $name Name of a product from 4 to 100 characters length
 * @param float $price Price of a product
 * @param float $weight Weight of a book
 */
class Book extends Product
{
	private const TYPE = 2;
	private $weight;

	public function __construct($id, $sku, $name, $price, $weight)
	{
		$this->setWeight($weight);
		parent::__construct($id, $sku, $name, $price);
	}

	public function getWeight()
	{
		return $this->weight;
	}

	public function setWeight($weight)
	{
		if (!is_numeric($weight)) throw new InvalidArgumentException("DVD-Disc size must be a numeric value");
		else if (floatval($weight) <= 0) throw new RangeException("DVD-Disct size must be positive value");
		$this->weight = floatval($weight);
	}

	/*
		Function creates INSERT query to create product in database
		I had two ideas how to remove db dependency from product class
		Either provide it in this function or generate INSERT query
		But the last descision seemed to be better for me
	*/
	public function create()
	{
		return "INSERT INTO products (sku, name, price, type, extra) VALUES "
			."(\"$this->sku\", \"$this->name\", $this->price, ".Book::TYPE.", \"$this->weight\")";
	}
}