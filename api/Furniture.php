<?php
/**
 * This is a class implemented to deal with products
 * 
 * @param int|null $id id of a product which must be null if it is a new product
 * @param string $sku SKU of a product from 0 to 14 characters length
 * @param string $name Name of a product from 4 to 100 characters length
 * @param float $price Price of a product
 * @param string $dimensions Dimensions of a furniture in HxWxL format
 */
class Furniture extends Product
{
	private const TYPE = 3;
	private $dimensions;

	public function __construct($id, $sku, $name, $price, $dimensions)
	{
		$this->setDimensions($dimensions);
		parent::__construct($id, $sku, $name, $price);
	}

	public function getDimensions()
	{
		return $this->dimensions;
	}

	public function setDimensions($dimensions)
	{
		$regArray = array();
		preg_match('/\d+\.?\d*x\d+\.?\d*x\d+\.?\d*/', $dimensions, $regArray); // Regex which checks pattern of HxWxL
		if (sizeof($regArray) <= 0 || $dimensions !== $regArray[0]) throw new InvalidArgumentException("Dimensions must be in form HxWxL");
		$this->dimensions = $dimensions;
	}

	/*
		Function creates INSERT query to create product in database
		I had two ideas how to remove db dependency from product class
		Either provide it in this function or generate INSERT query
		But the last descision seemed to be better for me
	*/
	public function create()
	{
		return "INSERT INTO products (sku, name, price, type, extra) VALUES "
			."(\"$this->sku\", \"$this->name\", $this->price, ".Furniture::TYPE.", \"$this->dimensions\")";
	}
}