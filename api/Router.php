<?php

// A quick implementation of a Router object class with GET and POST method support
class Router
{
	private $request;
	private $methods = array("GET", "POST");

	function __construct(IRequest $request)
	{
		$this->request = $request;
	}

	function __call($name, $args)
	{
		list($route, $method) = $args;

		if(!in_array(strtoupper($name), $this->methods)) $this->invalidMethodHandler();

		$this->{strtolower($name)}[$this->formatRoute($route)] = $method;
	}

	private function formatRoute($route)
	{
		$result = rtrim(str_replace('/scandiweb/api', '', $route), '/');

		if ($result === '') return '/';
		return $result;
	}

	private function invalidMethodHandler()
	{
		header("{$this->request->serverProtocol} 405 Method Not Allowed");
	}

	private function defaultRequestHanlder()
	{
		header("{$this->request->serverProtocol} 404 Not Found");
	}

	function resolve()
	{
		$methodDictionary = $this->{strtolower($this->request->requestMethod)};
		$formatedRoute = $this->formatRoute($this->request->requestUri);
		$method = $methodDictionary[$formatedRoute];

		if (is_null($method))
		{
			$this->defaultRequestHanlder();
			return;
		}

		echo call_user_func_array($method, array($this->request));
	}

	function __destruct()
	{
		$this->resolve();
	}
}