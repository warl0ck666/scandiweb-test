<?php
/**
 * This is an abstract class of e general product
 * 
 * @param int|null $id id of a product which must be null if it is a new product
 * @param string $sku SKU of a product from 0 to 14 characters length
 * @param string $name Name of a product from 4 to 100 characters length
 * @param float $price Price of a product
 */
abstract class Product
{
	protected $id;
	protected $sku;
	protected $name;
	protected $price;

	function __construct($id, $sku, $name, $price)
	{
		$this->setId($id);
		$this->setSku($sku);
		$this->setName($name);
		$this->setPrice($price);
	}

	public function getId()
	{
		return $this->id;
	}

	public function getSku()
	{
		return $this->sku;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function setId($id)
	{
		if (is_null($id)) $this->id = null;
		else if (is_numeric($id)) $this->id = intval($id);
		else throw new InvalidArgumentException("Product id must be either int or null");
	}

	public function setSku($sku)
	{
		if (strlen($sku) <= 0 || strlen($sku) > 14) throw new LengthException("Length of SKU must be between 0 and 14 characters");
		$this->sku = addslashes($sku);
	}

	public function setName($name)
	{
		if (strlen($name) < 4 || strlen($name) > 100) throw new LengthException("Length of name must be between 4 and 100 characters");
		$this->name = addslashes($name);
	}

	public function setPrice($price)
	{
		if (!is_numeric($price)) throw new InvalidArgumentException("Product price must be numeric value");
		else if (floatval($price) <= 0) throw new RangeException("Product price must be positive value");
		$this->price = floatval($price);
	}

	// Function which generates query to find product according to id, sku or both
	public static function find($id = null, $sku = null)
	{
		$query = "SELECT * FROM products WHERE";
		if (is_null($id) && is_null($sku)) throw new InvalidArgumentException("\$id and \$sku cannot be null at the same time");

		if (!is_null($id) && !is_null($sku)) $query = $query." id = $id AND sku = \"$sku\"";
		else if (!is_null($id)) $query = $query." id = $id";
		else $query = $query." sku = \"$sku\"";

		return $query;
	}

	abstract public function create();
}