function onTypeChange(value)
{
	const type = +value
	if (!type) return;

	const container = document.getElementById("type-extra");

	switch (type) {
		case 1:
			container.innerHTML = "<div class=\"regular-container\">"
				+ "<label><b>Size (MB): </b></label>"
				+ "<input id=\"size\" type=\"number\" />"
				+ "</div>";
			break;
		case 2:
			container.innerHTML = "<div class=\"regular-container\">"
			+ "<label><b>Weight (kg): </b></label>"
			+ "<input id=\"weight\" type=\"number\" />"
			+ "</div>";
			break;
		case 3:
			container.innerHTML = "<div class=\"regular-container\">"
			+ "<label><b>Height: </b></label>"
			+ "<input id=\"height\" type=\"number\" />"
			+ "</div>";
			container.innerHTML += "<div class=\"regular-container\">"
			+ "<label><b>Width: </b></label>"
			+ "<input id=\"width\" type=\"number\" />"
			+ "</div>";
			container.innerHTML += "<div class=\"regular-container\">"
			+ "<label><b>Length: </b></label>"
			+ "<input id=\"length\" type=\"number\" />"
			+ "</div>";
			break;
		default: 
			return;
	}
}

// Main function to create new product, which checks every condition
function addNewProduct()
{
	const errorParagraph = document.getElementById("error");
	if (errorParagraph) errorParagraph.innerText = "";

	const sku = document.getElementById("sku").value;
	if (sku.length <= 0 || sku.length > 14) {
		displayError("incorrect_sku");
		return;
	}
	const name = document.getElementById("name").value;
	if (name.length <= 4) {
		displayError("incorrect_name");
		return;
	}
	const price = +document.getElementById("price").value;
	if (price <= 0) {
		displayError("incorrect_price");
		return;
	}
	const type = +document.getElementById("type").value;
	let extraInfo;

	switch (type) {
		case 1:
			extraInfo = +document.getElementById("size").value;
			if (extraInfo <= 0) {
				displayError("incorrect_size");
				return;
			}
			break;
		case 2:
			extraInfo = +document.getElementById("weight").value;
			if (extraInfo <= 0) {
				displayError("incorrect_weight");
				return;
			}
			break;
		case 3:
			const width = +document.getElementById("width").value;
			const height = +document.getElementById("height").value;
			const length = +document.getElementById("length").value;
			if (width <= 0 || length <= 0 || height <= 0) {
				displayError("incorrect_dimensions");
				return; 
			}
			extraInfo = height + "x" + width + "x" + length;
			break
		default: return;
	}

	sendRequest(sku, name, price, extraInfo, type).then((data) => {
		if (Number.isInteger(+data)) window.location.replace("/scandiweb/product/list");
	}).catch((error) => {
		if (error.responseMessage === "SKU already exists") displayError("sku_exists");
	});
}

// Promisified request to asynchronously create product in a db 
function sendRequest(sku, name, price, extraInfo, type) {
	return new Promise(function (resolve, reject) {
		const xhr = new XMLHttpRequest();
		xhr.open("POST", "/scandiweb/api/product");
		xhr.withCredentials = true;
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.onload = function () {
			if (this.status >= 200 && this.status <= 300) resolve(xhr.response);
			else {
				reject({ status: this.status, statusMessage: xhr.statusText, responseMessage: xhr.response });
			}
		};
		xhr.onerror = function () {
			reject({ status: this.status, statusMessage: xhr.statusText, responseMessage: xhr.response });
		}
		xhr.send(`sku=${sku}&name=${name}&price=${price}&extra=${extraInfo}&type=${type}`);
	})
}

// Error handling function
function displayError(error) {
	let errorParagraph = document.getElementById("error");
	if (!errorParagraph) document.getElementById("container").innerHTML = "<p id=\"error\"></p>" + document.getElementById("container").innerHTML;
	errorParagraph = document.getElementById("error");

	switch (error) {
		case "incorrect_name":
			errorParagraph.innerText = "Name must be longer than 4 characters";
			break;
		case "incorrect_sku":
			errorParagraph.innerText = "SKU must consist at least one character and must not be longer than 14 characters";
			break;
		case "incorrect_price":
			errorParagraph.innerText = "Price must be positive number";
			break;
		case "incorrect_size":
			errorParagraph.innerText = "Size must be positive number";
			break;
		case "incorrect_weight":
			errorParagraph.innerText = "Weight must be positive number";
			break;
		case "incorrect_dimensions":
			errorParagraph.innerText = "All dimensions must be positive numbers";
			break;
		case "sku_exists":
			errorParagraph.innerText = "SKU already exists";
			break;
		default:
			return;
	}
}