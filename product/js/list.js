window.onload = function () {
	loadProductList().then((data) => {
		for (let i = 0; i < data.length; i++) {
			const { id, sku, name, price, extra, type } = data[i];
			appendNewProduct(id, sku, name, price, extra, type);
		}
	}).catch(error => console.error(error));
}

var productsToDelete = new Set(); // Select which consist ids of products to delete

// Promisified function to load all products from db
function loadProductList() {
	return new Promise(function (resolve, reject) {
		const xhr = new XMLHttpRequest();
		xhr.open("GET", "/scandiweb/api/products");
		xhr.withCredentials = true;
		xhr.onload = function () {
			if (this.status >= 200 && this.status <= 300) resolve(JSON.parse(xhr.response));
			else {
				reject({ status: this.status, message: xhr.statusText });
			}
		};
		xhr.onerror = function () {
			reject({ status: this.status, message: this.statusText });
		}
		xhr.send();
	});
}

// Function to append product at the end of the page
function appendNewProduct(id, sku, name, price, extra, type) {
	const extraType = [];
	extraType["DVD-Disc"] = ["Size: ", "MB"];
	extraType["Book"] = ["Weight: ", "kg"];
	extraType["Furniture"] = ["Dimension: ", ""];
	const container = document.getElementById("container");
	if (!container) return;

	container.innerHTML += "<div id=\"product" + id + "\" class=\"product-container\">"
		+ "<p>" + name + "</p>"
		+ "<p>SKU: " + sku + "</p>"
		+ "<p>Price: " + price + "$</p>"
		+ "<p>" + extraType[type][0] + extra + extraType[type][1] + "</p>"
		+ "<input id=\"" + id +"\" type=\"checkbox\" onclick=\"checkToDelete(this.id)\" />";
		+ "</div>";
}

function checkToDelete(id) {
	if (productsToDelete.has(+id)) productsToDelete.delete(+id);
	else productsToDelete.add(+id);
}

// Main function to delete products from the list
function deleteSelected() {
	if (productsToDelete.size <= 0) {
		alert("You have to select at least one product to delete");
		return;
	}

	sendDeleteRequest(JSON.stringify([...productsToDelete])).then((data) => {
		if (data === "Success") {
			const array = [...productsToDelete];

			for (let i = 0; i < array.length; i++) {
				const element = document.getElementById("product" + array[i]);
				element.parentNode.removeChild(element);
			}

			productsToDelete.clear();
		}
	}).catch(error => console.error(error));
}

// Promisified function to delete products from db
function sendDeleteRequest(jsonArray) {
	return new Promise(function (resolve, reject) {
		const xhr = new XMLHttpRequest();
		xhr.open("POST", "/scandiweb/api/products/delete");
		xhr.withCredentials = true;
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.onload = function () {
			if (this.status >= 200 && this.status <= 300) resolve(xhr.response);
			else {
				reject({ status: this.status, statusMessage: xhr.statusText, responseMessage: xhr.response });
			}
		};
		xhr.onerror = function () {
			reject({ status: this.status, statusMessage: xhr.statusText, responseMessage: xhr.response });
		}
		xhr.send(`ids=${jsonArray}`);
	})
}