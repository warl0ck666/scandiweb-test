<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" type="text/css" href="css/new.css" />
	<script type="text/javascript" src="js/new.js"></script>
	<title>Add New Product</title>
</head>
<body>
	<header>
		<div id="header-div">
			<h1>Product Add</h1>
		</div>
		<hr>
	</header>
	<div id="container">
		<div>
			<div class="regular-container">
				<label><b>SKU: </b></label>
				<input id="sku" />
			</div>
			<div class="regular-container">
				<label><b>Name: </b></label>
				<input id="name" />
			</div>
			<div class="regular-container">
				<label><b>Price ($): </b></label>
				<input id="price" type="number" />
			</div>
			<div class="regular-container">
				<label><b>Type: </b></label>
				<select id="type" onchange="onTypeChange(this.value)">
					<option value="1">DVD-Disc</option>
					<option value="2">Book</option>
					<option value="3">Furniture</option>
				</select>
			</div>
		</div>
		<div id="type-extra">
			<div class="regular-container">
				<label><b>Size (MB): </b></label>
				<input id="size" type="number" />
			</div>
		</div>
		<div id="button-container">
			<button onclick="addNewProduct()">Save</button>
		</div>
	</div>
</body>
</html>