<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Product List</title>
	<script type="text/javascript" src="js/list.js"></script>
	<link rel="stylesheet" type="text/css" href="css/list.css" />
</head>
<body>
	<header>
		<div id="header-div">
			<div><h1>Product List</h1></div>
			<div id="functions">
				<button onclick="deleteSelected()">Delete</button>
				<button onclick="window.location.href = '/scandiweb/product/new'">Add</button>
			</div>
		</div>
		<hr>
	</header>
	<div id="container">
	</div>
</body>
</html>